#include <stdio.h>  // prints
#include <stdlib.h> // malloc
#include <string.h> // strcmp
#include <unistd.h> // read
#include <fcntl.h>  // open
#include <unistd.h> // close
#include <time.h>   // time

char password[32];
#define PASS_LEN 26
#define FLAG_LEN 75

void readPassword() {
	int fd = open("./password.txt", 0);
	read(fd, password, PASS_LEN);
	close(fd);
}

void printFlag(char *buf) {
	int fd = open("flag.txt", 0);
	char pbuff[FLAG_LEN+1];
	memset(pbuff, 0, FLAG_LEN+1);

	// Reads the password into .data
	readPassword();

	//if (!strcmp(buf, password)) {
	if (!memcmp(buf, password, PASS_LEN)) {
		read(fd, pbuff, FLAG_LEN);
		printf("%s\n", pbuff);
	} else {
		puts("Try harder");
	}

	close(fd);
}

void pwnme() {
	printf("Enter pancake password\n> ");
	char buf[32];
	read(0, buf, 64);

	printFlag(buf);

}


int main() {
    setvbuf(stdout, NULL, _IONBF, 20);
    setvbuf(stdin, NULL, _IONBF, 20);

	pwnme();

    return 0;
}
