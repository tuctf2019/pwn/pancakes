# pancakes

Desc: `You ever just get a craving for pancakes?`

Architecture: x86

Given files:

* pancakes

Hints:

* Where is the flag when it's read?

Flag: `TUCTF{p4nc4k35_4r3_4b50lu73ly_d3l1c10u5_4nd_y0u_5h0uld_637_50m3_4f73r_7h15}`
